resource "aws_s3_bucket" "badBucket" {
  bucket = "myBucket"
  acl = "authenticated-read"
  versioning {
        		enabled = true
    		}
}
